namespace Collection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMoneyProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Banknotes", "Nominal", c => c.Int(nullable: false));
            AddColumn("dbo.Banknotes", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Banknotes", "Circulation", c => c.Int(nullable: false));
            AddColumn("dbo.Coins", "Nominal", c => c.Int(nullable: false));
            AddColumn("dbo.Coins", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Coins", "Circulation", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Coins", "Circulation");
            DropColumn("dbo.Coins", "Year");
            DropColumn("dbo.Coins", "Nominal");
            DropColumn("dbo.Banknotes", "Circulation");
            DropColumn("dbo.Banknotes", "Year");
            DropColumn("dbo.Banknotes", "Nominal");
        }
    }
}
