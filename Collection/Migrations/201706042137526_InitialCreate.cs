namespace Collection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Banknotes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Country = c.String(),
                        Material = c.String(),
                        ImgSrc = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Coins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Diameter = c.Int(nullable: false),
                        Country = c.String(),
                        Material = c.String(),
                        ImgSrc = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Coins");
            DropTable("dbo.Banknotes");
        }
    }
}
