namespace Collection.Data_Layer_Access
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CollectionContext : DbContext
    {
        public CollectionContext()
            : base("name=CollectionContext")
        {
        }

        public DbSet<Banknote> banknotes { get; set; }
        public DbSet<Coin> coins { get; set; }
    }

}