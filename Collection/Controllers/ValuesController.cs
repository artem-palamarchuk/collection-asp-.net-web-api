﻿using Collection.Data_Layer_Access;
using Collection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Collection.Controllers
{
    public class ValuesController : ApiController
    {
        // GET: api/Values
        public List<Banknote> Get()
        {
            using (CollectionContext db = new CollectionContext())
            {
                var banknotes = db.banknotes.ToList();
                return banknotes;
            }
        }

        // GET: api/Values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Values
        public void Post(Banknote banknote)
        {
            using(CollectionContext db = new CollectionContext())
            {
                db.banknotes.Add(banknote);
                db.SaveChanges();
            }
        }

        // PUT: api/Values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Values/5
        public void Delete(int id)
        {
        }
    }
}
