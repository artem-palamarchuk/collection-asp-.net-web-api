﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection.Models
{
    interface IProduct : ICategory
    {
        string ProductName { get; set; }
        int Article { get; set; }
        int Price { get; set; }
    }
}
