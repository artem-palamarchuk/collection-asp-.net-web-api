﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collection.Models
{
    public abstract class CollectItem : BaseForID
    {
        public abstract string Material { get; set; }
        public abstract string Country { get; set; }
        public abstract string ImgSrc { get; set; }
        public abstract string Description { get; set; }
    }
}