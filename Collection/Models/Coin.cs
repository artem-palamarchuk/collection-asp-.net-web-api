﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collection.Models
{
    public class Coin : Money, IDiameter
    {
        private int CoinID;
        public override int ID
        {
            get { return CoinID; }
            set { CoinID = value; }
        }

        public int Diameter { get; set; }
        public override int Nominal { get; set; }
        public override int Year { get; set; }
        public override int Circulation { get; set; }
        public override string Country { get; set; }
        public override string Material { get; set; }
        public override string ImgSrc { get; set; }
        public override string Description { get; set; }

        public Coin() { }
    }
}