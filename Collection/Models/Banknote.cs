﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collection.Models
{
    public class Banknote : Money, IDimensions
    {
        private int BanknoteID;
        public override int ID
        {
            get { return BanknoteID; }
            set { BanknoteID = value; }
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public override int Nominal { get; set; }
        public override int Year { get; set; }
        public override int Circulation { get; set; }
        public override string Country { get; set; }
        public override string Material { get; set; }
        public override string ImgSrc { get; set; }
        public override string Description { get; set; }

        public Banknote() { }
    }
}