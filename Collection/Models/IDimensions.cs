﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection.Models
{
    interface IDimensions
    {
        int Width { get; set; }
        int Height { get; set; }
    }
}
