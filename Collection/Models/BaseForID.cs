﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collection.Models
{
    public abstract class BaseForID
    {
        public abstract int ID { get; set; }
    }
}