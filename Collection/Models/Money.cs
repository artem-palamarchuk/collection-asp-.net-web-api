﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collection.Models
{
    public abstract class Money : CollectItem
    {
        public abstract int Nominal { get; set; }
        public abstract int Year { get; set; }
        public abstract int Circulation { get; set; }
    }
}