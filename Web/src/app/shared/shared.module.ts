﻿import { NgModule } from '@angular/core';

import { HttpService, SettingsService } from './index';

@NgModule({
    imports: [],
    declarations: [],
    providers: [
        HttpService,
        SettingsService
    ]
})
export class SharedModule { }
