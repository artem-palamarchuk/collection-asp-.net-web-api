﻿import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import { HttpService } from './index';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    constructor(
        private httpService: HttpService,
    ) { }

    banknotes: Array<any>;
    captions: Array<string> = [
        "Ширина",
        "Высота",
        "Номинал",
        "Год выпуска",
        "Тираж",
        "Страна",
        "Материал",
        "Изображение",
        "Описание"
    ]

    /* User fields */
    width: FormControl = new FormControl(100, [Validators.required]);
    height: FormControl = new FormControl(50, [Validators.required]);
    nominal: FormControl = new FormControl(10, [Validators.required]);
    year: FormControl = new FormControl(1945, [Validators.required]);
    circulation: FormControl = new FormControl(10000, [Validators.required]);
    country: FormControl = new FormControl("Украина", [Validators.required]);
    material: FormControl = new FormControl("Бумага", [Validators.required]);
    imgSrc: FormControl = new FormControl("Пока ничего", [Validators.required]);
    description: FormControl = new FormControl("Тут будет описание", [Validators.required]);

    banknotesForm: FormGroup = new FormGroup({
        "width": this.width,
        "height": this.height,
        "nominal": this.nominal,
        "year": this.year,
        "circulation": this.circulation,
        "country": this.country,
        "material": this.material,
        "image": this.imgSrc,
        "description": this.description
    })

    url: string = "api/values";

    public AddData() {
        let queryData = {
            width: this.width.value,
            height: this.height.value,
            nominal: this.nominal.value,
            year: this.year.value,
            circulation: this.circulation.value,
            country: this.country.value,
            material: this.material.value,
            imgSrc: this.imgSrc.value,
            description: this.description.value
        };
        this.httpService.post(this.url, queryData).subscribe(data => {
            console.log("Success post!!!")
        });
    }

    public LoadData() {
        this.httpService.get(this.url).subscribe(data => {
            this.banknotes = data.json();
        });
    }

}
